#include "Player.h"
#include "TextureUtils.h"
#include <stdexcept>
//#include "TextureUtils.cpp"

/**
 * initPlayer
 * 
 * Function to populate an animation structure from given paramters. 
 * 
 * @param player Player structure to populate 
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @return void?  Could do with returning a value for success/error 
 */
void Player::init(SDL_Renderer* renderer)
{
    // Create player texture from file, optimised for renderer
    texture = createTextureFromFile("assets/images/undeadking.png", renderer);

    if (texture == nullptr) throw std::runtime_error("File not found!");

    // Allocate memory for the animation structures
    for (int i = 0; i < MAX_ANIMATIONS; i++)
    {
        animations[i] = new Animation();
    }
    // Setup the animation structure
    animations[LEFT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[RIGHT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[UP]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[DOWN]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
}

Player::Player()
{
    speed = 50.0f;

    // position
    x = 250.0f;
    y = 250.0f;

    // V
    vx = 0.0f;
    vy = 0.0f;

    state = IDLE;

    texture = nullptr;

    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;

    for (int i = 0; i < MAX_ANIMATIONS; ++i) {
        animations[i] = nullptr;
    }
}

/**
 * destPlayer
 * 
 * Function to clean up an player structure.  
 * 
 * @param player Player structure to destroy
 */
Player::~Player()
{
    // Clean up animations - free memory
    for (int i = 0; i < MAX_ANIMATIONS; i++)
    {
        // Clean up the animaton structure
        // allocated with new so use delete.
        delete animations[i];
        animations[i] = nullptr;
    }
    // Clean up
    SDL_DestroyTexture(texture);
    texture = nullptr;
}

/**
 * drawPlayer
 * 
 * Function draw a Player structure
 * 
 * @param player Player structure tp draw
 * @param renderer SDL_Renderer to draw to
 */
void Player::draw(SDL_Renderer* renderer)
{
    // Get current animation based on the state. 
    Animation* current = this->animations[state];
    SDL_RenderCopy(renderer,
        texture,
        current->getCurrentFrame(),
        &targetRectangle);
}

/**
 * processInput
 * 
 * Function to process inputs for the player structure
 * Note: Need to think about other forms of input!
 * 
 * @param player Player structure processing the input
 * @param keyStates The keystates array. 
 */
void Player::processInput(const Uint8 *keyStates)
{
    // Process Player Input
    // 
    //Input - keys/joysticks?

    float verticalInput = 0.0f;
    float horizontalInput = 0.0f;


    // If no keys are down player should not animate!
    state = IDLE;

    // This could be more complex, e.g. increasing the vertical
    // input while the key is held down.
    if (keyStates[SDL_SCANCODE_UP])
    {
        verticalInput = -1.0f;
        state = UP;
    }
    if (keyStates[SDL_SCANCODE_DOWN])
    {
        verticalInput = 1.0f;
        state = DOWN;
    }
    if (keyStates[SDL_SCANCODE_RIGHT])
    {
        horizontalInput = 1.0f;
        state = RIGHT;
    }
    if (keyStates[SDL_SCANCODE_LEFT])
    {
        horizontalInput = -1.0f;
        state = LEFT;
    }

    // Calculate player velocity.
    // Note: This is imperfect, no account taken of diagonal!
    vy = verticalInput * speed;
    vx = horizontalInput * speed;
}

/**
 * updatePlayer
 * 
 * Function to update the player structure and its components
 * 
 * @param player Player structure being updated
 * @param timeDeltaInSeconds the time delta in seconds
 */

void Player::update(float timeDeltaInSeconds)
{
    // Calculate distance travelled since last update
    float yMovement = timeDeltaInSeconds * vy;
    float xMovement = timeDeltaInSeconds * vx;

    // Update player position.
    x += xMovement;
    y += yMovement;

    // Move sprite to nearest pixel location.
    targetRectangle.y = round(y);
    targetRectangle.x = round(x);

    // Get current animation
    Animation* current = animations[state];

    // let animation update itself.
    current->update(timeDeltaInSeconds);
}

